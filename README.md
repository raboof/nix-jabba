# nix-jabba

For development purposes it can be convenient to test against various specific
JDK versions. This project provides a convenient way to quickly fetch many of the JDK's
provided by 'jabba'.

## usage

From nix-shell:

```
{ pkgs ? import <nixpkgs> {} }:

let
  jabba = pkgs.callPackage (fetchGit {
    url = "https://codeberg.org/raboof/nix-jabba";
    ref = "main";
  }) { };
in
pkgs.mkShell {
  name = "nix-jabba-demo";
  buildInputs = [
    jabba.temurin."18"
  ];
}
```
