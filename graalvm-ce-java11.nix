{ stdenv
, fetchurl
, patchelf
, glibc
, xorg
, zlib
}:

# File generated by ./update.py, do not edit

{
  "21.1.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "21.1.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.1.0/graalvm-ce-java11-linux-amd64-21.1.0.tar.gz";
      sha256 = "1dsabrdzw9z82mb6416fzpzhwsij7awzhsa2rv4dn5nbs9a2j99r";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "21.0.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "21.0.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0.2/graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz";
      sha256 = "0d7rprbip8yvss6zwlxnrninyw9ximc734q8rpjlyg40drwvwgxx";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.3.2" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.3.2";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.3.2/graalvm-ce-java11-linux-amd64-20.3.2.tar.gz";
      sha256 = "1cjlv61z3k28msvyj36rcphh51xc7j2c93l8z2ff0xv2sm5d39pi";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.3.1" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.3.1";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.3.1.2/graalvm-ce-java11-linux-amd64-20.3.1.2.tar.gz";
      sha256 = "1p6ig67h6myxfpgf350ycn5xb88ql4rfvddbwx0cdx1kd02w9r75";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.3.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.3.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.3.0/graalvm-ce-java11-linux-amd64-20.3.0.tar.gz";
      sha256 = "";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.2.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.2.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.2.0/graalvm-ce-java11-linux-amd64-20.2.0.tar.gz";
      sha256 = "0aaf0sjsnlckhgsh3j4lph0shahw6slf4yndqcm2swc8i1dlpdsx";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.1.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.1.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.1.0/graalvm-ce-java11-linux-amd64-20.1.0.tar.gz";
      sha256 = "154kzj9966hjwkspndb79n4ylidn5ll8w6alppawqricclcxrwhq";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "20.0.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "20.0.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.0.0/graalvm-ce-java11-linux-amd64-20.0.0.tar.gz";
      sha256 = "09n7fk2cp49b3472fnlwh0fsdrwyyjvawk3m6s4xj6a618s4lv6i";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.6" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.6";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.6/graalvm-ce-java11-linux-amd64-19.3.6.tar.gz";
      sha256 = "0wmdsg7w73a9wd1kizylcn1f95xlsv1b1y9k9gdn7gl9ds11cncr";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.5" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.5";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.5/graalvm-ce-java11-linux-amd64-19.3.5.tar.gz";
      sha256 = "15dw1cs9rckhr7rgphs1rfp8ikfi9dvjj6glgzgyj0fmikscsdj9";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.4" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.4";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.4/graalvm-ce-java11-linux-amd64-19.3.4.tar.gz";
      sha256 = "1x3d6j2srsyca59mkklnpwwf17f9s744s1i5y9bprbqyjp8xf075";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.3" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.3";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.3/graalvm-ce-java11-linux-amd64-19.3.3.tar.gz";
      sha256 = "0x7mj4c7yq3i3yyak22jv9l8ldwsq3j7fvkhzgqlmvm8s7rhndxy";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.2" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.2";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.2/graalvm-ce-java11-linux-amd64-19.3.2.tar.gz";
      sha256 = "1c6m29apgmii7d8q14x37jxihnqisd7zwzlk7lz787rl246a89vn";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.1" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.1";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.1/graalvm-ce-java11-linux-amd64-19.3.1.tar.gz";
      sha256 = "0zcy5hg2s0pfgsir86g57nksbf3fbs98fq4ia3xs0jswqxvha7v9";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
  "19.3.0" = (
    let result = stdenv.mkDerivation rec {
    pname = "jdk-jabba-graalvm-ce-java11";
    version = "19.3.0";
    src = fetchurl {
      url = "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.0.2/graalvm-ce-java11-linux-amd64-19.3.0.2.tar.gz";
      sha256 = "19zaii2l76i0w0r52bxnp3q1bvlzvpwkyrzzi2h18ywpikmni2gj";
    };
    buildPhase = "";
    installPhase = ''
      mkdir -p $out
      mv * $out
      for f in $out/bin/* $out/jre/bin/*; do
        ${patchelf}/bin/patchelf --set-interpreter ${glibc}/lib/ld-linux-x86-64.so.2 --set-rpath "$out/lib:$out/lib/jli:$out/lib/amd64/jli:${zlib}/lib:${xorg.libXext}/lib" $f || true
      done
      for f in $out/lib/*; do
        ${patchelf}/bin/patchelf --set-rpath $out/lib:${zlib}/lib:${xorg.libXext}/lib:${xorg.libX11}/lib:${xorg.libXrender}/lib:${xorg.libXtst}/lib:${xorg.libXi}/lib $f || true
      done
    '';
    passthru.home = result;
    passthru.jre = result;
    preferLocalBuild = true;
  }; in result);
}
