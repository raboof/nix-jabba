{ pkgs ? import <nixpkgs> {} }:

let
  jabba = pkgs.callPackage ./default.nix { };
in
pkgs.mkShell {
  name = "nix-jabba-demo";
  buildInputs = [
    jabba.openjdk."1.15.0-1"
  ];
}
