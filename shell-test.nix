{ pkgs ? import <nixpkgs> {} }:

let
  jabba = pkgs.callPackage (fetchGit {
    url = "https://git.sr.ht/~raboof/nix-jabba";
    ref = "main";
  }) { };
in
pkgs.mkShell {
  name = "nix-jabba-demo";
  buildInputs = [
    jabba.adopt."1.14.0-2"
  ];
}
